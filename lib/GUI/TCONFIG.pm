# Copyright (c) Stephan Martin <sm@sm-zone.net>
#
# $Id: TCONFIG.pm,v 1.6 2006/06/28 21:50:42 sm Exp $
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.

use strict;
package GUI::TCONFIG;

use POSIX;

use Glib qw(TRUE FALSE);

#
# main screen for configuration
#
sub show_configbox {
   my ($main, $name) = @_;

   if(not defined($name)) {
      $name = $main->{'CA'}->{'actca'};
   }
   if(not defined($name)) {
      GUI::HELPERS::print_warning(_("Can't get CA name"));
      return;
   }

   $main->{'TCONFIG'}->init_config($main, $name);

   # ================================================================
   # Dialog window.
   # ================================================================
   my ($box, $button_ok, $button_apply, $button_cancel, $button_help,
       $content_box, $notebook, $buttonbox, $t);

   $box = Gtk3::Window->new("toplevel");
   $box->set_title("OpenSSL Configuration");
   $box->set_resizable(1);
   $box->set_default_size(800, 600);
   $box->signal_connect(
       'delete_event' => sub {
           $box->destroy()
       });

   $button_ok = Gtk3::Button->new_from_stock('gtk-ok');
   $button_ok->set_sensitive(0);
   $button_ok->signal_connect(
       'clicked' => sub {
           $main->{'TCONFIG'}->write_config($main, $name);
           $box->destroy()
       });
   $box->{'button_ok'} = $button_ok;

   $button_apply = Gtk3::Button->new_from_stock('gtk-apply');
   $button_apply->set_sensitive(0);
   $button_apply->signal_connect(
       'clicked' => sub {
           $main->{'TCONFIG'}->write_config($main, $name)
       });
   $box->{'button_apply'} = $button_apply;

   $button_cancel = Gtk3::Button->new_from_stock('gtk-cancel');
   $button_cancel->signal_connect(
       'clicked' => sub {
           $box->destroy()
       });

   $t = _("All Settings are written unchanged to openssl.conf.\nSo please study the documentation of OpenSSL if you don't know exactly what to do.\nIf you are still unsure - keep the defaults and everything is expected to work fine.");
   $button_help = Gtk3::Button->new_from_stock('gtk-help');
   $button_help->signal_connect(
       'clicked' => sub {
           GUI::HELPERS::print_info($t)
       });

   $content_box = Gtk3::VBox->new();

   $notebook = Gtk3::Notebook->new();
   $notebook->set_tab_pos('top');
   $notebook->set_show_tabs(1);
   $notebook->set_show_border(1);
   $notebook->set_scrollable(0);

   $content_box->pack_start($notebook, TRUE, TRUE, 0);

   $box->{'notebook'} = $notebook;

   $buttonbox = Gtk3::HButtonBox->new();
   $buttonbox->set_layout('end');
   $buttonbox->set_spacing(3);
   $buttonbox->set_border_width(3);
   $buttonbox->add($button_help);
   $buttonbox->set_child_secondary($button_help, 1);

   $buttonbox->add($button_ok);
   $buttonbox->add($button_apply);
   $buttonbox->add($button_cancel);

   $content_box->pack_start($buttonbox, FALSE, FALSE, 0);

   $box->add($content_box);

   # ================================================================
   # First page: vbox with warnings :-)
   # ================================================================
   {
       my ($pagebox, $label, $separator);

       $pagebox = Gtk3::VBox->new(0, 0);

       $label = GUI::HELPERS::create_label(
           _("OpenSSL Configuration"), 'center', 0,0);

       $notebook->append_page($pagebox, $label);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("OpenSSL Configuration"), 'center', 0, 1);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $separator = Gtk3::HSeparator->new();
       $pagebox->pack_start($separator, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("Only change these options, if you really know, what you are doing!!"),
           'center', 1, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("You should be aware, that some options may break some crappy software!!"),
           'center', 1, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);


       $label = GUI::HELPERS::create_label(
           _("If you are unsure: leave the defaults untouched"),
           'center', 1, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);
   }

   # ================================================================
   # Second page: Server settings
   # ================================================================
   {
       my (@options, @special_options, @options_ca, $pagebox, $label,
           $separator, $table, $rows, $entry);

       @options = qw(
         nsComment
         crlDistributionPoints
         authorityKeyIdentifier
         issuerAltName
         nsBaseUrl
         nsCaPolicyUrl
           );

       @special_options = qw(
         nsCertType
         nsSslServerName
         nsRevocationUrl
         nsRenewalUrl
         subjectAltName
         keyUsage
         extendedkeyUsage
           );

       @options_ca = qw(
         default_days
           );

       $pagebox = Gtk3::VBox->new(0, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("These Settings are passed to OpenSSL for creating Server Certificates"),
           'center', 0, 1);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("Multiple Values can be separated by \",\""),
           'center', 1, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $separator = Gtk3::HSeparator->new();
       $pagebox->pack_start($separator, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $rows = 1;
       $table = Gtk3::Table->new($rows, 2, 0);
       $pagebox->pack_start($table, 1, 1, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(_("Server Certificate Settings"),
                                           'center', 0, 0);
       $label = Gtk3::Label->new(_("Server Certificate Settings"));

       $notebook->append_page($pagebox, $label);

       # --------------------------------
       # Special option server subjectAltName
       # --------------------------------
       {
           my ($subjectAltNameType, $subjectAltName, $label, $radiobox, $rb1,
               $rb2, $rb3, $combosubjectAltName, @combostrings, $tmp);

           $subjectAltNameType = \$main->{'TCONFIG'}->{'server_cert'}->{'subjectAltNameType'};
           $subjectAltName = \$main->{'TCONFIG'}->{'server_cert'}->{'subjectAltName'};

           # subjectAltNameType: Create radiobuttons.
           $label = GUI::HELPERS::create_label(
               _("Subject alternative name (subjectAltName):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           $radiobox = Gtk3::HBox->new(0, 0);
           $rb1 = Gtk3::RadioButton->new_with_label_from_widget(
               undef, _($main->{'words'}{'ip'}));
           $rb1->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb1, $subjectAltNameType, 'ip', $box)
               });
           $radiobox->add($rb1);

           $rb2 = Gtk3::RadioButton->new_with_label_from_widget(
               $rb1, _($main->{'words'}{'dns'}));
           $rb2->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb2, $subjectAltNameType, 'dns', $box)
               });
           $radiobox->add($rb2);

           $rb3 = Gtk3::RadioButton->new_with_label_from_widget(
               $rb1, _($main->{'words'}{'raw'}));
           $rb3->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb3, $subjectAltNameType, 'raw', $box)
               });
           $radiobox->add($rb3);

           $rb1->set_active(1) if ($$subjectAltNameType eq 'ip');
           $rb2->set_active(1) if ($$subjectAltNameType eq 'dns');
           $rb3->set_active(1) if ($$subjectAltNameType eq 'raw');

           # subjectAltName: Create and populate combobox,
           $combosubjectAltName = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none user emailcopy)); # First entry is default.
           foreach my $id (@combostrings) {
               $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combosubjectAltName->append($id, $label);
           }

           # ...connect signals,
           $combosubjectAltName->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combosubjectAltName, $subjectAltName)
               });
           $tmp = [@combostrings[1..$#combostrings]];  # "Rest" of list @combostrings.
           $combosubjectAltName->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_radio_group_sensitive_if_selected_in_list(
                       $rb1, $combosubjectAltName, $tmp)
               });

           # ...select default entry,
           if (defined($$subjectAltName)
               && grep {$$subjectAltName eq $_} @combostrings) {
               $combosubjectAltName->set_active_id($$subjectAltName);
           } else {
               $combosubjectAltName->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combosubjectAltName, 1, 2, $rows-1, $rows);
           $rows++;

           $table->attach_defaults($radiobox, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option server keyUsage
       # --------------------------------
       {
           my ($keyUsageType, $keyUsage, $label, $radiobox, $rb1, $rb2,
               $combokeyUsage, @combostrings, $tmp);

           $keyUsageType = \$main->{'TCONFIG'}->{'server_cert'}->{'keyUsageType'};
           $keyUsage = \$main->{'TCONFIG'}->{'server_cert'}->{'keyUsage'};

           $label = GUI::HELPERS::create_label(
               _("Key Usage (keyUsage):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           $radiobox = Gtk3::HBox->new(0, 0);

           $rb1 = Gtk3::RadioButton->new_with_label_from_widget(
               undef, _($main->{'words'}{'critical'}));
           $rb1->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb1, $keyUsageType, 'critical', $box)
               });
           $radiobox->add($rb1);

           $rb2 = Gtk3::RadioButton->new_with_label_from_widget(
               $rb1, _($main->{'words'}{'noncritical'}));
           $rb2->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb2, $keyUsageType, 'noncritical', $box)
               });
           $radiobox->add($rb2);

           $rb1->set_active(1) if ($$keyUsageType eq 'critical');
           $rb2->set_active(1) if ($$keyUsageType eq 'noncritical');

           # keyUsage: Create and populate combobox,
           $combokeyUsage = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none sig key keysig)); # First entry is default.
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combokeyUsage->append($id, $label);
           }

           # ...connect signals,
           $combokeyUsage->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combokeyUsage, $keyUsage)
               });
           $tmp = [@combostrings[1..$#combostrings]];  # "Rest" of list @combostrings.
           $combokeyUsage->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_radio_group_sensitive_if_selected_in_list(
                       $rb1, $combokeyUsage, $tmp)
               });

           # ...select default entry,
           if (defined($$keyUsage)
               && grep {$$keyUsage eq $_} @combostrings) {
               $combokeyUsage->set_active_id($$keyUsage);
           } else {
               $combokeyUsage->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combokeyUsage, 1, 2, $rows-1, $rows);
           $rows++;

           $table->attach_defaults($radiobox, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option server extendedKeyUsage
       # --------------------------------
       {
           my ($extendedKeyUsageType, $extendedKeyUsage, $label, $radiobox,
               $rb1, $rb2, $comboextendedKeyUsage, @combostrings, $tmp);

           $extendedKeyUsageType = \$main->{'TCONFIG'}->{'server_cert'}->{'extendedKeyUsageType'};
           $extendedKeyUsage = \$main->{'TCONFIG'}->{'server_cert'}->{'extendedKeyUsage'};

           $label = GUI::HELPERS::create_label(
               _("Extended Key Usage (extendedKeyUsage):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           $radiobox = Gtk3::HBox->new(0, 0);
           $rb1 = Gtk3::RadioButton->new_with_label_from_widget(
               undef, _($main->{'words'}{'critical'}));
           $rb1->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb1, $extendedKeyUsageType, 'critical', $box)
               });
           $radiobox->add($rb1);

           $rb2 = Gtk3::RadioButton->new_with_label_from_widget(
               $rb1, _($main->{'words'}{'noncritical'}));
           $rb2->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb2, $extendedKeyUsageType, 'noncritical', $box)
               });
           $radiobox->add($rb2);

           $rb1->set_active(1) if ($$extendedKeyUsageType eq 'critical');
           $rb2->set_active(1) if ($$extendedKeyUsageType eq 'noncritical');

           # extendedKeyUsage: Create and populate combobox,
           $comboextendedKeyUsage = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none user));
           # (Add custom value for extendedKeyUsage to combobox options.)
           if ((defined($$extendedKeyUsage)) &&
               ($$extendedKeyUsage ne 'none') && ($$extendedKeyUsage ne '')) {
               push(@combostrings, $$extendedKeyUsage);
           }
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $comboextendedKeyUsage->append($id, $label);
           }

           # ...connect signals,
           $comboextendedKeyUsage->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $comboextendedKeyUsage, $extendedKeyUsage)
               });
           $tmp = [@combostrings[1..$#combostrings]];  # "Rest" of list @combostrings.
           $comboextendedKeyUsage->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_radio_group_sensitive_if_selected_in_list(
                       $rb1, $comboextendedKeyUsage, $tmp)
               });

           # ...select default entry,
           if (defined($$extendedKeyUsage)
               && grep {$$extendedKeyUsage eq $_} @combostrings) {
               $comboextendedKeyUsage->set_active_id($$extendedKeyUsage);
           } else {
               $comboextendedKeyUsage->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($comboextendedKeyUsage, 1, 2, $rows-1, $rows);
           $rows++;

           $table->attach_defaults($radiobox, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option server nsCerttype
       # --------------------------------
       {
           my ($label, $nsCertType, $combonsCertType, @combostrings);

           $nsCertType = \$main->{'TCONFIG'}->{'server_cert'}->{'nsCertType'};

           $label = GUI::HELPERS::create_label(
               _("Netscape Certificate Type (nsCertType):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           # nsCertType: Create and populate combobox,
           $combonsCertType = Gtk3::ComboBoxText->new();
           @combostrings = ('none', 'server', 'server, client'); # First entry is default.
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combonsCertType->append($id, $label);
           }

           # ...connect signals,
           $combonsCertType->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combonsCertType, $nsCertType)
               });

           # ...select default entry,
           if (defined($$nsCertType)
               && grep {$$nsCertType eq $_} @combostrings) {
               $combonsCertType->set_active_id($$nsCertType);
           } else {
               $combonsCertType->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combonsCertType, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option server nsSslServer
       # --------------------------------
       {
           my ($label, $nsSslServer, $combonsSslServer, @combostrings);

           $nsSslServer = \$main->{'TCONFIG'}->{'server_cert'}->{'nsSslServerName'};

           $label = GUI::HELPERS::create_label(
               _("Netscape SSL Server Name (nsSslServerName):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           # nsSslServer: Create and populate combobox,
           $combonsSslServer = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none user));
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combonsSslServer->append($id, $label);
           }

           # ...connect signals,
           $combonsSslServer->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combonsSslServer, $nsSslServer)
               });

           # ...select default entry,
           if (defined($$nsSslServer)
               && grep {$$nsSslServer eq $_} @combostrings) {
               $combonsSslServer->set_active_id($$nsSslServer);
           } else {
               $combonsSslServer->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combonsSslServer, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option server nsRevocationUrl
       # --------------------------------
       {
           my ($label, $nsRevocationUrl, $combonsRevocationUrl, @combostrings);

           $nsRevocationUrl = \$main->{'TCONFIG'}->{'server_cert'}->{'nsRevocationUrl'};

           $label = GUI::HELPERS::create_label(
               _("Netscape Revocation URL (nsRevocationUrl):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           # nsRevocationUrl: Create and populate combobox,
           $combonsRevocationUrl = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none user));
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combonsRevocationUrl->append($id, $label);
           }

           # ...connect signals,
           $combonsRevocationUrl->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combonsRevocationUrl, $nsRevocationUrl)
               });

           # ...select default entry,
           if (defined($$nsRevocationUrl)
               && grep {$$nsRevocationUrl eq $_} @combostrings) {
               $combonsRevocationUrl->set_active_id($$nsRevocationUrl);
           } else {
               $combonsRevocationUrl->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combonsRevocationUrl, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option server nsRenewalUrl
       # --------------------------------
       {
           my ($label, $nsRenewalUrl, $combonsRenewalUrl, @combostrings);

           $nsRenewalUrl = \$main->{'TCONFIG'}->{'server_cert'}->{'nsRenewalUrl'};

           $label = GUI::HELPERS::create_label(
               _("Netscape Renewal URL (nsRenewalUrl):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           # nsRenewalUrl: Create and populate combobox,
           $combonsRenewalUrl = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none user));
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combonsRenewalUrl->append($id, $label);
           }

           # ...connect signals,
           $combonsRenewalUrl->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combonsRenewalUrl, $nsRenewalUrl)
               });

           # ...select default entry,
           if (defined($$nsRenewalUrl)
               && grep {$$nsRenewalUrl eq $_} @combostrings) {
               $combonsRenewalUrl->set_active_id($$nsRenewalUrl);
           } else {
               $combonsRenewalUrl->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combonsRenewalUrl, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Standard server options
       # --------------------------------
       foreach my $key (@options) {
           $entry = GUI::HELPERS::entry_to_table(
               "$key:", \$main->{'TCONFIG'}->{'server_cert'}->{$key},
               $table, $rows-1, 1, $box);

           $rows++;
           $table->resize($rows, 2);
       }

       foreach my $key (@options_ca) {
           $entry = GUI::HELPERS::entry_to_table(
               "$key:", \$main->{'TCONFIG'}->{'server_ca'}->{$key},
               $table, $rows-1, 1, $box);

           $rows++;
           $table->resize($rows, 2);
       }
   }

   # ================================================================
   # Third page: Client settings
   # ================================================================
   {
       my (@options, @options_ca, $pagebox, $label, $separator, $table, $rows,
           $entry);

       @options = qw(
         nsComment
         crlDistributionPoints
         authorityKeyIdentifier
         issuerAltName
         nsBaseUrl
         nsCaPolicyUrl
           );

       @options_ca = qw(
         default_days
           );
       $pagebox = Gtk3::VBox->new(0, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("These Settings are passed to OpenSSL for creating Client Certificates"),
           'center', 0, 1);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("Multiple Values can be separated by \",\""),
           'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $separator = Gtk3::HSeparator->new();
       $pagebox->pack_start($separator, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $rows = 1;
       $table = Gtk3::Table->new($rows, 2, 0);
       $pagebox->pack_start($table, 1, 1, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(_("Client Certificate Settings"),
                                           'center', 0, 0);
       $notebook->append_page($pagebox, $label);

       # --------------------------------
       # Special option client subjectAltName
       # --------------------------------
       {
           my ($subjectAltNameType, $subjectAltName, $label, $radiobox, $rb1,
               $rb2, $rb3, $rb4, $combosubjectAltName, @combostrings, $tmp);

           $subjectAltNameType = \$main->{'TCONFIG'}->{'client_cert'}->{'subjectAltNameType'};
           $subjectAltName = \$main->{'TCONFIG'}->{'client_cert'}->{'subjectAltName'};

           $label = GUI::HELPERS::create_label(
               _("Subject alternative name (subjectAltName):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           $radiobox = Gtk3::HBox->new(0, 0);
           $rb1 = Gtk3::RadioButton->new_with_label_from_widget(
               undef, _($main->{'words'}{'ip'}));
           $rb1->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb1, $subjectAltNameType, 'ip', $box)
               });
           $radiobox->add($rb1);

           $rb2 = Gtk3::RadioButton->new_with_label_from_widget(
               $rb1, _($main->{'words'}{'dns'}));
           $rb2->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb2, $subjectAltNameType, 'dns', $box)
               });
           $radiobox->add($rb2);

           $rb3 = Gtk3::RadioButton->new_with_label_from_widget(
               $rb1, _($main->{'words'}{'mail'}));
           $rb3->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb3, $subjectAltNameType, 'mail', $box)
               });
           $radiobox->add($rb3);

           $rb4 = Gtk3::RadioButton->new_with_label_from_widget(
               $rb1, _($main->{'words'}{'raw'}));
           $rb4->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb4, $subjectAltNameType, 'raw', $box)
               });
           $radiobox->add($rb4);

           $rb1->set_active(1) if ($$subjectAltNameType eq 'ip');
           $rb2->set_active(1) if ($$subjectAltNameType eq 'dns');
           $rb3->set_active(1) if ($$subjectAltNameType eq 'mail');
           $rb4->set_active(1) if ($$subjectAltNameType eq 'raw');

           # csubjectAltName: Create and populate combobox,
           $combosubjectAltName = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none user emailcopy)); # First entry is default.
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combosubjectAltName->append($id, $label);
           }

           # ...connect signals,
           $combosubjectAltName->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combosubjectAltName, $subjectAltName)
               });
           $tmp = [@combostrings[1..$#combostrings]];  # "Rest" of list @combostrings.
           $combosubjectAltName->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_radio_group_sensitive_if_selected_in_list(
                       $rb1, $combosubjectAltName, $tmp)
               });

           # ...select default entry,
           if (defined($$subjectAltName)
               && grep {$$subjectAltName eq $_} @combostrings) {
               $combosubjectAltName->set_active_id($$subjectAltName);
           } else {
               $combosubjectAltName->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combosubjectAltName, 1, 2, $rows-1, $rows);
           $rows++;

           $table->attach_defaults($radiobox, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option client keyUsage
       # --------------------------------
       {
           my ($keyUsageType, $keyUsage, $label, $radiobox, $rb1, $rb2,
               $combokeyUsage, @combostrings, $tmp);

           $keyUsageType = \$main->{'TCONFIG'}->{'client_cert'}->{'keyUsageType'};
           $keyUsage = \$main->{'TCONFIG'}->{'client_cert'}->{'keyUsage'};

           $label = GUI::HELPERS::create_label(
               _("Key Usage (keyUsage):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           $radiobox = Gtk3::HBox->new(0, 0);
           $rb1 = Gtk3::RadioButton->new_with_label_from_widget(
               undef, _($main->{'words'}{'critical'}));
           $rb1->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb1, $keyUsageType, 'critical', $box)
               });
           $radiobox->add($rb1);

           $rb2 = Gtk3::RadioButton->new_with_label_from_widget(
               $rb1, _($main->{'words'}{'noncritical'}));
           $rb2->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb2, $keyUsageType, 'noncritical', $box)
               });
           $radiobox->add($rb2);

           $rb1->set_active(1) if ($$keyUsageType eq 'critical');
           $rb2->set_active(1) if ($$keyUsageType eq 'noncritical');

           # keyUsage: Create and populate combobox,
           $combokeyUsage = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none sig key keysig)); # First entry is default.
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combokeyUsage->append($id, $label);
           }

           # ...connect signals,
           $combokeyUsage->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combokeyUsage, $keyUsage)
               });
           $tmp = [@combostrings[1..$#combostrings]];  # "Rest" of list @combostrings.
           $combokeyUsage->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_radio_group_sensitive_if_selected_in_list(
                       $rb1, $combokeyUsage, $tmp)
               });

           # ...select default entry,
           if (defined($$keyUsage)
               && grep {$$keyUsage eq $_} @combostrings) {
               $combokeyUsage->set_active_id($$keyUsage);
           } else {
               $combokeyUsage->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combokeyUsage, 1, 2, $rows-1, $rows);
           $rows++;

           $table->attach_defaults($radiobox, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option client extendedKeyUsage
       # --------------------------------
       {
           my ($extendedKeyUsageType, $extendedKeyUsage, $label, $radiobox,
               $rb1, $rb2, $comboextendedKeyUsage, @combostrings, $tmp);

           $extendedKeyUsageType = \$main->{'TCONFIG'}->{'client_cert'}->{'extendedKeyUsageType'};
           $extendedKeyUsage = \$main->{'TCONFIG'}->{'client_cert'}->{'extendedKeyUsage'};

           $label = GUI::HELPERS::create_label(
               _("Extended Key Usage (extendedKeyUsage):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           $radiobox = Gtk3::HBox->new(0, 0);
           $rb1 = Gtk3::RadioButton->new_with_label_from_widget(
               undef, _($main->{'words'}{'critical'}));
           $rb1->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb1, $extendedKeyUsageType, 'critical', $box)
               });
           $radiobox->add($rb1);

           $rb2 = Gtk3::RadioButton->new_with_label_from_widget(
               $rb1, _($main->{'words'}{'noncritical'}));
           $rb2->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb2, $extendedKeyUsageType, 'noncritical', $box)
               });
           $radiobox->add($rb2);

           $rb1->set_active(1) if ($$extendedKeyUsageType eq 'critical');
           $rb2->set_active(1) if ($$extendedKeyUsageType eq 'noncritical');

           # extendedKeyUsage: Create and populate combobox,
           $comboextendedKeyUsage = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none user)); # First entry is default.
           # (Add custom value for extendedKeyUsage to combobox options.)
           if ((defined($$extendedKeyUsage)) &&
               ($$extendedKeyUsage ne 'none') && ($$extendedKeyUsage ne '')) {
               push(@combostrings, $$extendedKeyUsage);
           }
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $comboextendedKeyUsage->append($id, $label);
           }

           # ...connect signals,
           $comboextendedKeyUsage->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $comboextendedKeyUsage, $extendedKeyUsage)
               });
           $tmp = [@combostrings[1..$#combostrings]];  # "Rest" of list @combostrings.
           $comboextendedKeyUsage->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_radio_group_sensitive_if_selected_in_list(
                       $rb1, $comboextendedKeyUsage, $tmp)
               });

           # ...select default entry,
           if (defined($$extendedKeyUsage)
               && grep {$$extendedKeyUsage eq $_} @combostrings) {
               $comboextendedKeyUsage->set_active_id($$extendedKeyUsage);
           } else {
               $comboextendedKeyUsage->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($comboextendedKeyUsage, 1, 2, $rows-1, $rows);
           $rows++;

           $table->attach_defaults($radiobox, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option client nsCertType
       # --------------------------------
       {
           my ($nsCertType, $label, $combonsCertType, @combostrings);

           $nsCertType = \$main->{'TCONFIG'}->{'client_cert'}->{'nsCertType'};

           $label = GUI::HELPERS::create_label(
               _("Netscape Certificate Type (nsCertType):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           # nsCertType: Create and populate combobox,
           $combonsCertType = Gtk3::ComboBoxText->new();
           @combostrings = ( # First entry is default.
                             'none',
                             'objsign',
                             'email',
                             'client',
                             'client, email',
                             'client, objsign',
                             'client, email, objsign');
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combonsCertType->append($id, $label);
           }

           # ...connect signals,
           $combonsCertType->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combonsCertType, $nsCertType)
               });

           # ...select default entry,
           if (defined($$nsCertType)
               && grep {$$nsCertType eq $_} @combostrings) {
               $combonsCertType->set_active_id($$nsCertType);
           } else {
               $combonsCertType->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combonsCertType, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option client nsRevocationUrl
       # --------------------------------
       {
           my ($nsRevocationUrl, $label, $combonsRevocationUrl, @combostrings);

           $nsRevocationUrl = \$main->{'TCONFIG'}->{'client_cert'}->{'nsRevocationUrl'};

           $label = GUI::HELPERS::create_label(
               _("Netscape Revocation URL (nsRevocationUrl):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           # nsRevocationUrl: Create and populate combobox,
           $combonsRevocationUrl = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none user)); # First entry is default.
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combonsRevocationUrl->append($id, $label);
           }

           # ...connect signals,
           $combonsRevocationUrl->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combonsRevocationUrl, $nsRevocationUrl)
               });

           # ...select default entry,
           if (defined($$nsRevocationUrl)
               && grep {$$nsRevocationUrl eq $_} @combostrings) {
               $combonsRevocationUrl->set_active_id($$nsRevocationUrl);
           } else {
               $combonsRevocationUrl->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combonsRevocationUrl, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option client nsRenewalUrl
       # --------------------------------
       {
           my ($nsRenewalUrl, $combonsRenewalUrl, $label, @combostrings);

           $nsRenewalUrl = \$main->{'TCONFIG'}->{'client_cert'}->{'nsRenewalUrl'};

           $label = GUI::HELPERS::create_label(
               _("Netscape Renewal URL (nsRenewalUrl):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           # nsRenewalUrl: Create and populate combobox,
           $combonsRenewalUrl = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none user)); # First entry is default.
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combonsRenewalUrl->append($id, $label);
           }

           # ...connect signals,
           $combonsRenewalUrl->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combonsRenewalUrl, $nsRenewalUrl)
               });

           # ...select default entry,
           if (defined($$nsRenewalUrl)
               && grep {$$nsRenewalUrl eq $_} @combostrings) {
               $combonsRenewalUrl->set_active_id($$nsRenewalUrl);
           } else {
               $combonsRenewalUrl->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combonsRenewalUrl, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Standard client options
       # --------------------------------
       foreach my $key (@options) {
           $entry = GUI::HELPERS::entry_to_table(
               "$key:", \$main->{'TCONFIG'}->{'client_cert'}->{$key},
               $table, $rows-1, 1, $box);

           $rows++;
           $table->resize($rows, 2);
       }

       foreach my $key (@options_ca) {
           $entry = GUI::HELPERS::entry_to_table(
               "$key:", \$main->{'TCONFIG'}->{'client_ca'}->{$key},
               $table, $rows-1, 1, $box);

           $rows++;
           $table->resize($rows, 2);
       }
   }

   # ================================================================
   # Fourth page: CA settings
   # ================================================================
   {
       my (@options, @special_options, @options_ca, $pagebox, $label,
           $separator, $table, $rows, $entry);

       @options = qw(
         nsComment
         crlDistributionPoints
         authorityKeyIdentifier
         issuerAltName
         nsBaseUrl
         nsCaPolicyUrl
           );

       @special_options = qw(
         nsCertType
         nsRevocationUrl
         subjectAltName
           );

       @options_ca = qw(
         default_days
           );
       $pagebox = Gtk3::VBox->new(0, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("These Settings are passed to OpenSSL for creating CA Certificates"),
           'center', 0, 1);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("Multiple Values can be separated by \",\""),
           'center', 1, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $separator = Gtk3::HSeparator->new();
       $pagebox->pack_start($separator, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $rows = 1;
       $table = Gtk3::Table->new($rows, 2, 0);
       $pagebox->pack_start($table, 1, 1, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(_("CA Certificate Settings"),
                                           'center', 0, 0);
       $label = Gtk3::Label->new(_("CA Certificate Settings"));

       $notebook->append_page($pagebox, $label);

       # --------------------------------
       # Special option CA subjectAltName
       # --------------------------------
       {
           my ($subjectAltName, $label, $combosubjectAltName, @combostrings);

           $subjectAltName = \$main->{'TCONFIG'}->{'v3_ca'}->{'subjectAltName'};

           $label = GUI::HELPERS::create_label(
               _("Subject alternative name (subjectAltName):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           # subjectAltName: Create and populate combobox,
           $combosubjectAltName = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none emailcopy)); # First entry is default.
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combosubjectAltName->append($id, $label);
           }

           # ...connect signals,
           $combosubjectAltName->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combosubjectAltName, $subjectAltName)
               });

           # ...select default entry,
           if (defined($$subjectAltName)
               && grep {$$subjectAltName eq $_} @combostrings) {
               $combosubjectAltName->set_active_id($$subjectAltName);
           } else {
               $combosubjectAltName->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combosubjectAltName, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option CA nsCerttype
       # --------------------------------
       {
           my ($nsCertType, $label, $combonsCertType, @combostrings);

           $nsCertType = \$main->{'TCONFIG'}->{'v3_ca'}->{'nsCertType'};

           $label = GUI::HELPERS::create_label(
               _("Netscape Certificate Type (nsCertType):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           # nsCertType: Create and populate combobox,
           $combonsCertType = Gtk3::ComboBoxText->new();
           @combostrings = ( # First entry is default.
                             'none',
                             'emailCA',
                             'sslCA',
                             'objCA',
                             'sslCA, emailCA',
                             'sslCA, objCA',
                             'emailCA, objCA',
                             'sslCA, emailCA, objCA');
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combonsCertType->append($id, $label);
           }

           # ...connect signals,
           $combonsCertType->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combonsCertType, $nsCertType)
               });

           # ...select default entry,
           if (defined($$nsCertType)
               && grep {$$nsCertType eq $_} @combostrings) {
               $combonsCertType->set_active_id($$nsCertType);
           } else {
               $combonsCertType->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combonsCertType, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # Special option CA keyUsage
       # --------------------------------
       {
           my ($keyUsageType, $keyUsage, $label, $radiobox, $rb1, $rb2,
               $combokeyUsage, @combostrings, $tmp);

           $keyUsageType = \$main->{'TCONFIG'}->{'v3_ca'}->{'keyUsageType'};
           $keyUsage = \$main->{'TCONFIG'}->{'v3_ca'}->{'keyUsage'};

           $label = GUI::HELPERS::create_label(
               _("Key Usage (keyUsage):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           $radiobox = Gtk3::HBox->new(0, 0);
           $rb1 = Gtk3::RadioButton->new_with_label_from_widget(
               undef, _($main->{'words'}{'critical'}));
           $rb1->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb1, $keyUsageType, 'critical', $box)
               });
           $radiobox->add($rb1);

           $rb2 = Gtk3::RadioButton->new_with_label_from_widget(
               $rb1, _($main->{'words'}{'noncritical'}));
           $rb2->signal_connect(
               'toggled' => sub {
                   GUI::CALLBACK::toggle_to_var_pref(
                       $rb2, $keyUsageType, 'noncritical', $box)
               });
           $radiobox->add($rb2);

           $rb1->set_active(1) if ($$keyUsageType eq 'critical');
           $rb2->set_active(1) if ($$keyUsageType eq 'noncritical');

           # keyUsage: Create and populate combobox,
           $combokeyUsage = Gtk3::ComboBoxText->new();
           @combostrings = ( # First entry is default.
                             'none',
                             'keyCertSign',
                             'cRLSign',
                             'keyCertSign, cRLSign');
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combokeyUsage->append($id, $label);
           }

           # ...connect signals,
           $combokeyUsage->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combokeyUsage, $keyUsage)
               });
           $tmp = [@combostrings[1..$#combostrings]];  # "Rest" of list @combostrings.
           $combokeyUsage->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_radio_group_sensitive_if_selected_in_list(
                       $rb1, $combokeyUsage, $tmp)
               });


           # ...select default entry,
           if (defined($$keyUsage)
               && grep {$$keyUsage eq $_} @combostrings) {
               $combokeyUsage->set_active_id($$keyUsage);
           } else {
               $combokeyUsage->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combokeyUsage, 1, 2, $rows-1, $rows);
           $rows++;

           $table->attach_defaults($radiobox, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # special option nsRevocationUrl
       # --------------------------------
       {
           my ($nsRevocationUrl, $label, $combonsRevocationUrl, @combostrings);

           $nsRevocationUrl = \$main->{'TCONFIG'}->{'v3_ca'}->{'nsRevocationUrl'};

           $label = GUI::HELPERS::create_label(
               _("Netscape Revocation URL (nsRevocationUrl):"), 'left', 0, 0);
           $table->attach_defaults($label, 0, 1, $rows-1, $rows);

           # nsRevocationUrl: Create and populate combobox,
           $combonsRevocationUrl = Gtk3::ComboBoxText->new();
           @combostrings = (qw(none user)); # First is default.
           foreach my $id (@combostrings) {
               my $label = exists $main->{'words'}{$id} ? $main->{'words'}{$id} : $id;
               $combonsRevocationUrl->append($id, $label);
           }

           # ...connect signals,
           $combonsRevocationUrl->signal_connect(
               'changed' => sub {
                   GUI::CALLBACK::combo_box_assign_selected(
                       $combonsRevocationUrl, $nsRevocationUrl)
               });

           # ...select default entry,
           if (defined($$nsRevocationUrl)
               && grep {$$nsRevocationUrl eq $_} @combostrings) {
               $combonsRevocationUrl->set_active_id($$nsRevocationUrl);
           } else {
               $combonsRevocationUrl->set_active_id($combostrings[0]);
           }

           # ...and add to dialog.
           $table->attach_defaults($combonsRevocationUrl, 1, 2, $rows-1, $rows);
           $rows++;
       }

       # --------------------------------
       # standard options
       # --------------------------------
       foreach my $key (@options) {
           $entry = GUI::HELPERS::entry_to_table(
               "$key:", \$main->{'TCONFIG'}->{'v3_ca'}->{$key},
               $table, $rows-1, 1, $box);

           $rows++;
           $table->resize($rows, 2);
       }

       foreach my $key (@options_ca) {
           $entry = GUI::HELPERS::entry_to_table(
               "$key:", \$main->{'TCONFIG'}->{'ca_ca'}->{$key},
               $table, $rows-1, 1, $box);

           $rows++;
           $table->resize($rows, 2);
       }
   }

   # ================================================================
   # Fifth page: CRL settings
   # ================================================================
   {
       my (@options, $pagebox, $label, $separator, $table, $rows, $entry);

       @options = qw(
         default_crl_days
           );

       $pagebox = Gtk3::VBox->new(0, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("These Settings are passed to OpenSSL for creating Certificate Revocation Lists"),
           'center', 0, 1);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("Multiple Values can be separated by \",\""),
           'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $separator = Gtk3::HSeparator->new();
       $pagebox->pack_start($separator, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $rows = 1;
       $table = Gtk3::Table->new($rows, 2, 0);
       $pagebox->pack_start($table, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
       $pagebox->pack_start($label, FALSE, FALSE, 0);

       $label = GUI::HELPERS::create_label(
           _("Revocation List Settings"), 'center', 0, 0);
       $notebook->append_page($pagebox, $label);

       foreach my $key (@options) {
           $entry = GUI::HELPERS::entry_to_table(
               "$key:", \$main->{'TCONFIG'}->{'server_ca'}->{$key},
               $table, $rows-1, 1, $box);

           $rows++;
           $table->resize($rows, 2);
       }
   }

   # ================================================================
   # Show dialog.
   # ================================================================
   $box->show_all();

   $button_ok->set_sensitive(FALSE);
   $button_apply->set_sensitive(FALSE);

   return;
}

#
# configuration for CA
#
sub show_config_ca {
   my ($main, $opts, $mode) = @_;

   my(@options, $key, $box, $button_ok, $button_cancel, $table, $label,
         $entry, $rows, @combostrings, $combonsCertType, $combosubjectAltName,
         $combokeyUsage);

   @options = qw(
         authorityKeyIdentifier
         basicConstraints
         issuerAltName
         nsComment
         nsCaRevocationUrl
         nsCaPolicyUrl
         nsRevocationUrl
         nsPolicyUrl
         );
   
   if(not defined($opts->{'name'})) {
      GUI::HELPERS::print_warning(_("Can't get CA name"));
      return;
   }

   # Read openssl.cnf for this CA
   $main->{'TCONFIG'}->init_config($main, $opts->{'name'});

   $button_ok = Gtk3::Button->new_from_stock('gtk-ok');
   $button_ok->set_can_default(TRUE);

   $button_ok->signal_connect('clicked', 
         sub { 
            $main->{'TCONFIG'}->write_config($main, $opts->{'name'});
            $opts->{'configured'} = 1;
            $main->{'CA'}->create_ca($main, $opts, $box, $mode) });


   $button_cancel = Gtk3::Button->new_from_stock('gtk-cancel');
   $button_cancel->signal_connect('clicked', sub { $box->destroy() });

   $box = GUI::HELPERS::dialog_box(
         _("CA Configuration"), _("CA Configuration"), 
         $button_ok, $button_cancel);


   $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
   $box->get_content_area()->add($label);

   $label = GUI::HELPERS::create_label(
         _("These Settings are passed to OpenSSL for creating this CA Certificate"), 
         'center', 0, 1);
   $box->get_content_area()->add($label);

   $label = GUI::HELPERS::create_label(
         _("and the CA Certificates of every SubCA, created with this CA."),
         'center', 0, 1);
   $box->get_content_area()->add($label);
   
   $label = GUI::HELPERS::create_label(
         _("Multiple Values can be separated by \",\""),
         'center', 0, 0);
   $box->get_content_area()->add($label);
   
   $label = GUI::HELPERS::create_label(' ', 'center', 0, 0);
   $box->get_content_area()->add($label);

   $label = GUI::HELPERS::create_label(
         _("If you are unsure: leave the defaults untouched"), 
         'center', 0, 0);
   $box->get_content_area()->add($label);

   $rows = 1;
   $table = Gtk3::Table->new($rows, 2, 0);
   $box->get_content_area()->add($table);

   # special option keyUsage
   $label = GUI::HELPERS::create_label(
         _("Key Usage (keyUsage):"), 'left', 0, 0);
   $table->attach_defaults($label, 0, 1, $rows-1, $rows);

   # Radio buttons to select between critical and noncritical status.
   # These are meant to be active only if key usage is not 'none'.
   $main->{'radiobox'} = Gtk3::HBox->new(0, 0);
   $main->{'radio1'} = Gtk3::RadioButton->new_from_widget(undef);
   $main->{'radio1'}->set_label(_($main->{'words'}{'critical'}));
   if($main->{'TCONFIG'}->{'v3_ca'}->{'keyUsageType'} eq 'critical') {
      $main->{'radio1'}->set_active(1)
   }
   $main->{'radio1'}->signal_connect('toggled' =>
         sub{ GUI::CALLBACK::toggle_to_var_pref( $main->{'radio1'}, 
            \$main->{'TCONFIG'}->{'v3_ca'}->{'keyUsageType'}, 'critical')});
   $main->{'radiobox'}->add($main->{'radio1'});

   $main->{'radio2'} = Gtk3::RadioButton->new_from_widget($main->{'radio1'});
   $main->{'radio2'}->set_label(_($main->{'words'}{'noncritical'}));
   if($main->{'TCONFIG'}->{'v3_ca'}->{'keyUsageType'} eq 'noncritical') {
      $main->{'radio2'}->set_active(1)
   }
   $main->{'radio2'}->signal_connect('toggled' =>
         sub {GUI::CALLBACK::toggle_to_var_pref($main->{'radio2'},
         \$main->{'TCONFIG'}->{'v3_ca'}->{'keyUsageType'}, 'noncritical')});
   $main->{'radiobox'}->add($main->{'radio2'});

   $combokeyUsage = Gtk3::ComboBoxText->new();
   @combostrings = ('none',
                    'keyCertSign',
                    'cRLSign',
                    'keyCertSign, cRLSign');
   foreach my $id (@combostrings) {
       $combokeyUsage->append($id, $main->{'words'}{$id});
   }

   $combokeyUsage->signal_connect(
       'changed' => sub {
           GUI::CALLBACK::combo_box_assign_selected(
               $combokeyUsage,
               \$main->{'TCONFIG'}->{'v3_ca'}->{'keyUsage'})
       });
   my @tmp = @combostrings[1..$#combostrings];  # "Rest" of list @combostrings.
   $combokeyUsage->signal_connect(
       'changed' => sub {
           GUI::CALLBACK::combo_box_radio_group_sensitive_if_selected_in_list(
               $main->{'radio1'}, $combokeyUsage, \@tmp)
       });
   if (defined($main->{'TCONFIG'}->{'v3_ca'}->{'keyUsage'})
       && grep {$main->{'TCONFIG'}->{'v3_ca'}->{'keyUsage'} eq $_} @combostrings) {
       $combokeyUsage->set_active_id($main->{'TCONFIG'}->{'v3_ca'}->{'keyUsage'});
   } else {
       $combokeyUsage->set_active_id('keyCertSign, cRLSign');
   }

   $table->attach_defaults($combokeyUsage, 1, 2, $rows-1, $rows);
   $rows++;

   $table->attach_defaults($main->{'radiobox'}, 1, 2, $rows-1, $rows);
   $rows++;

   # special option nsCerttype
   $label = GUI::HELPERS::create_label(
         _("Netscape Certificate Type (nsCertType):"), 'left', 0, 0);
   $table->attach_defaults($label, 0, 1, $rows-1, $rows);

   $combonsCertType = Gtk3::ComboBoxText->new();
   @combostrings = ('none',
                    'emailCA',
                    'sslCA',
                    'objCA',
                    'sslCA, emailCA',
                    'sslCA, objCA',
                    'emailCA, objCA',
                    'sslCA, emailCA, objCA');
   foreach my $id (@combostrings) {
       $combonsCertType->append($id, $main->{'words'}{$id});
   }

   $combonsCertType->signal_connect(
       'changed' => sub {
           GUI::CALLBACK::combo_box_assign_selected(
               $combonsCertType,
               \$main->{'TCONFIG'}->{'v3_ca'}->{'nsCertType'})
       });
   if (defined($main->{'TCONFIG'}->{'v3_ca'}->{'nsCertType'})
       && grep {$main->{'TCONFIG'}->{'v3_ca'}->{'nsCertType'} eq $_} @combostrings) {
       $combonsCertType->set_active_id($main->{'TCONFIG'}->{'v3_ca'}->{'nsCertType'});
   } else {
       $combonsCertType->set_active_id('none');
   }

   $table->attach_defaults($combonsCertType, 1, 2, $rows-1, $rows);
   $rows++;

   # special option subjectAltName
   $label = GUI::HELPERS::create_label(
         _("Subject alternative name (subjectAltName):"), 'left', 0, 0);
   $table->attach_defaults($label, 0, 1, $rows-1, $rows);

   $combosubjectAltName = Gtk3::ComboBoxText->new();
   @combostrings = ('none',
                    'emailcopy');
   foreach my $id (@combostrings) {
       $combosubjectAltName->append($id, $main->{'words'}{$id});
   }

   $combosubjectAltName->signal_connect(
       'changed' => sub {
           GUI::CALLBACK::combo_box_assign_selected(
               $combosubjectAltName,
               \$main->{'TCONFIG'}->{'v3_ca'}->{'subjectAltName'})
       });
   if (defined($main->{'TCONFIG'}->{'v3_ca'}->{'subjectAltName'})
       && grep {$main->{'TCONFIG'}->{'v3_ca'}->{'subjectAltName'} eq $_} @combostrings) {
       $combosubjectAltName->set_active_id($main->{'TCONFIG'}->{'v3_ca'}->{'subjectAltName'});
   } else {
       $combosubjectAltName->set_active_id('none');
   }

   $table->attach_defaults($combosubjectAltName, 1, 2, $rows-1, $rows);
   $rows++;

   foreach $key (@options) {
      $entry = GUI::HELPERS::entry_to_table("$key:",
            \$main->{'TCONFIG'}->{'v3_ca'}->{$key}, $table, $rows-1, 1);

      $rows++;
      $table->resize($rows, 2);
   }

   $box->show_all();

   return;
}

1
